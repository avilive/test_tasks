
        
create table DOCUMENTS
(
  id            NUMBER CONSTRAINT DOC_PK PRIMARY KEY,
  docreg_id     NUMBER not null,
  type          NUMBER,
  timestamp     date,
  user_id       number
);
------------

create table ADD_ORDERS
(
  id                NUMBER CONSTRAINT ORD_PK PRIMARY KEY,
  doc_id            NUMBER ,
  line              VARCHAR2(5),
  port_code         VARCHAR2(10)
);
  
alter table ADD_ORDERS
add CONSTRAINT ORD_DOC_FK foreign key(DOC_ID)  references DOCUMENTS (ID);

-------------- 
create table ADD_CONTAINERS
(
  id                  NUMBER CONSTRAINT CNT_PK PRIMARY KEY,
  crun_id             NUMBER,
  container_num       VARCHAR2(11),
  container_type      VARCHAR2(7)
);
--------------

create table LINK_ORD_CONT
(
  ord_id  NUMBER,
  cont_id NUMBER
);        
alter table LINK_ORD_CONT
add CONSTRAINT LOC_ORD_FK foreign key(ORD_ID)  references ADD_ORDERS (ID);
alter table LINK_ORD_CONT
add CONSTRAINT LOC_CNT_FK foreign key(CONT_ID)  references ADD_CONTAINERS (ID);

insert into add_containers (ID, CRUN_ID, CONTAINER_NUM, CONTAINER_TYPE)
values (2421, 487, 'JTFK952', 'CV');

insert into add_containers (ID, CRUN_ID, CONTAINER_NUM, CONTAINER_TYPE)
values (2422, 232, 'KQLG663', 'QT');

insert into add_containers (ID, CRUN_ID, CONTAINER_NUM, CONTAINER_TYPE)
values (2423, 647, 'GPJT360', 'PK');

insert into add_containers (ID, CRUN_ID, CONTAINER_NUM, CONTAINER_TYPE)
values (2424, 367, 'XLDX119', 'BZ');

insert into add_containers (ID, CRUN_ID, CONTAINER_NUM, CONTAINER_TYPE)
values (2425, 221, 'WJFJ571', 'TK');

insert into add_containers (ID, CRUN_ID, CONTAINER_NUM, CONTAINER_TYPE)
values (2426, 482, 'MKTD783', 'WZ');

insert into add_containers (ID, CRUN_ID, CONTAINER_NUM, CONTAINER_TYPE)
values (2427, 928, 'QRHV245', 'FS');

insert into add_containers (ID, CRUN_ID, CONTAINER_NUM, CONTAINER_TYPE)
values (2428, 649, 'FJID78', 'VX');

insert into add_containers (ID, CRUN_ID, CONTAINER_NUM, CONTAINER_TYPE)
values (2429, 127, 'ZQPM178', 'DG');

insert into add_containers (ID, CRUN_ID, CONTAINER_NUM, CONTAINER_TYPE)
values (2430, 867, 'ADWH218', 'JP');

insert into add_containers (ID, CRUN_ID, CONTAINER_NUM, CONTAINER_TYPE)
values (2431, 776, 'CLSS628', 'RT');

insert into add_containers (ID, CRUN_ID, CONTAINER_NUM, CONTAINER_TYPE)
values (2432, 24, 'SNMH782', 'HF');

insert into add_containers (ID, CRUN_ID, CONTAINER_NUM, CONTAINER_TYPE)
values (2433, 742, 'BJPX999', 'FP');

insert into add_containers (ID, CRUN_ID, CONTAINER_NUM, CONTAINER_TYPE)
values (2434, 319, 'UECY795', 'HW');

insert into add_containers (ID, CRUN_ID, CONTAINER_NUM, CONTAINER_TYPE)
values (2435, 146, 'UAWK256', 'QH');

insert into add_containers (ID, CRUN_ID, CONTAINER_NUM, CONTAINER_TYPE)
values (2436, 350, 'ECTX719', 'MF');

insert into add_containers (ID, CRUN_ID, CONTAINER_NUM, CONTAINER_TYPE)
values (2437, 524, 'NMFL302', 'EE');

insert into add_containers (ID, CRUN_ID, CONTAINER_NUM, CONTAINER_TYPE)
values (2438, 444, 'PVQV989', 'PI');

insert into add_containers (ID, CRUN_ID, CONTAINER_NUM, CONTAINER_TYPE)
values (2439, 246, 'OJSB467', 'NE');

insert into add_containers (ID, CRUN_ID, CONTAINER_NUM, CONTAINER_TYPE)
values (2440, 100, 'LJVL349', 'UR');



insert into documents (ID, DOCREG_ID, TYPE, TIMESTAMP, USER_ID)
values (2441, 652, 8, to_date('11-12-2018 11:57:40', 'dd-mm-yyyy hh24:mi:ss'), 68);

insert into documents (ID, DOCREG_ID, TYPE, TIMESTAMP, USER_ID)
values (2442, 837, 8, to_date('25-11-2018 11:57:40', 'dd-mm-yyyy hh24:mi:ss'), 27);

insert into documents (ID, DOCREG_ID, TYPE, TIMESTAMP, USER_ID)
values (2443, 192, 7, to_date('20-12-2018 11:57:40', 'dd-mm-yyyy hh24:mi:ss'), 22);

insert into documents (ID, DOCREG_ID, TYPE, TIMESTAMP, USER_ID)
values (2444, 885, 2, to_date('03-12-2018 11:57:40', 'dd-mm-yyyy hh24:mi:ss'), 17);

insert into documents (ID, DOCREG_ID, TYPE, TIMESTAMP, USER_ID)
values (2445, 316, 6, to_date('06-12-2018 11:57:40', 'dd-mm-yyyy hh24:mi:ss'), 85);

insert into documents (ID, DOCREG_ID, TYPE, TIMESTAMP, USER_ID)
values (2446, 674, 2, to_date('28-11-2018 11:57:40', 'dd-mm-yyyy hh24:mi:ss'), 29);

insert into documents (ID, DOCREG_ID, TYPE, TIMESTAMP, USER_ID)
values (2447, 962, 5, to_date('24-11-2018 11:57:40', 'dd-mm-yyyy hh24:mi:ss'), 15);

insert into documents (ID, DOCREG_ID, TYPE, TIMESTAMP, USER_ID)
values (2448, 301, 8, to_date('14-12-2018 11:57:40', 'dd-mm-yyyy hh24:mi:ss'), 98);

insert into documents (ID, DOCREG_ID, TYPE, TIMESTAMP, USER_ID)
values (2449, 585, 9, to_date('09-12-2018 11:57:40', 'dd-mm-yyyy hh24:mi:ss'), 4);

insert into documents (ID, DOCREG_ID, TYPE, TIMESTAMP, USER_ID)
values (2450, 71, 6, to_date('08-12-2018 11:57:40', 'dd-mm-yyyy hh24:mi:ss'), 27);

insert into add_orders (ID, DOC_ID, LINE, PORT_CODE)
values (119, 2441, 'LINE2', 'port2');

insert into add_orders (ID, DOC_ID, LINE, PORT_CODE)
values (120, 2442, 'LINE9', 'port2');

insert into add_orders (ID, DOC_ID, LINE, PORT_CODE)
values (121, 2443, 'LINE5', 'port3');

insert into add_orders (ID, DOC_ID, LINE, PORT_CODE)
values (122, 2444, 'LINE2', 'port2');

insert into add_orders (ID, DOC_ID, LINE, PORT_CODE)
values (123, 2445, 'LINE4', 'port4');

insert into add_orders (ID, DOC_ID, LINE, PORT_CODE)
values (124, 2446, 'LINE2', 'port1');

insert into add_orders (ID, DOC_ID, LINE, PORT_CODE)
values (125, 2447, 'LINE6', 'port3');

insert into add_orders (ID, DOC_ID, LINE, PORT_CODE)
values (126, 2448, 'LINE9', 'port3');

insert into add_orders (ID, DOC_ID, LINE, PORT_CODE)
values (127, 2449, 'LINE8', 'port2');

insert into add_orders (ID, DOC_ID, LINE, PORT_CODE)
values (128, 2450, 'LINE5', 'port5');

insert into link_ord_cont (ORD_ID, CONT_ID)
values (119, 2440);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (119, 2427);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (119, 2427);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (119, 2424);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (120, 2437);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (120, 2436);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (120, 2424);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (121, 2433);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (121, 2439);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (121, 2429);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (122, 2428);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (122, 2439);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (123, 2433);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (123, 2434);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (123, 2421);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (123, 2424);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (124, 2422);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (124, 2427);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (124, 2435);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (124, 2433);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (124, 2427);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (125, 2432);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (125, 2427);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (125, 2429);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (125, 2439);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (126, 2439);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (126, 2427);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (126, 2429);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (126, 2437);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (127, 2430);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (127, 2436);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (128, 2440);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (128, 2425);

insert into link_ord_cont (ORD_ID, CONT_ID)
values (128, 2428);

commit;
